# User Guide

## Setup
- Clone the repo
- add .env
- run composer install
- run npm install

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Add new VUE view

- Step 1:
    Go to `resources/js/views` and right click to create a New File. Give it a name and append `.vue` to it.
- Step 2:
    Every VUE file MUST have `<template></template>` tag. Inside this tag you can put your HTML (Bootstrap in this case).
- Step 3:
    After creating the VUE view you must declare it and set its route to the VUE router. The VUE router in this project is at `resources/js/app.js`.
    - First import the new file you just created under imports like this: import NewView from `./views/NewView` (If the name is NewView).
    - Then add the path you like your view to have inside routes array in this format:  `{
                                                                                            path: '/home/new-view-path',
                                                                                            name: 'newView',
                                                                                            component: NewView
                                                                                        }`
- Step 4:
    All you have to do now is make a link to this route at navigation bar or on-click event.

## Add new router link --> To link

- Step 1:
    Put this line of code to `<template></template>` section of the file.
    ` <router-link :to="{ name: 'about', params: { Id: Id } }">Navigate to About Page</router-link> `
- Step 2:
    To get the Id parameter you have to do this:
    `export default {
        data() {
            Id: 0
        },
        created() {
            this.Id = this.$route.params.Id;
        }
    }`

## Add new router link --> To Button

Put this line of code to `<template></template>` section of the file.
    ` <router-link :to="{ name: 'about', params: { Id: Id } }">Navigate to About Page</router-link> `


## Usefull Links

- [Pass Data Between Routes In A Vue.js Web Application](https://www.thepolyglotdeveloper.com/2017/11/pass-data-between-routes-vuejs-web-application/)

