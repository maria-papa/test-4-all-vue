<?php

namespace App\Http\Controllers;

use App\Employees;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class EmployeeController extends Controller
{

    public function getEmployees(Request $request)
    {
        $skip = $request->skip;
        $take = $request->take;
        $orderBy = $request->orderBy;

        $employees = DB::table('employees');

        if (!empty($orderby)) {
            $orderby = explode(' ', $orderby);
            if (empty($orderby[1])) {
                $employees = $employees->orderBy($orderby[0]);
            } else {
                $employees = $employees->orderBy($orderby[0],'desc');
            }
        }

        $employeeCount = $employees->count();

        $employees = $employees->skip($skip)->take($take)->get();
       
        
        return ['data' => $employees, 'count' => $employeeCount];
    }

}