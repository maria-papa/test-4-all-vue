// Vue & Vue-Router
import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter);


// Bootstrap
import BootstrapVue from 'bootstrap-vue';
Vue.use(BootstrapVue);

// Vue Resource
import VueResource from 'vue-resource';
Vue.use(VueResource);

// import router from './router'
// Vue.router = router

// Import new VUE pages HERE
import App from "./App";
import Home from "./pages/Home";
import ViewTemplate from "./pages/ViewTemplate";
import Users from "./pages/Users";
import Login from "./pages/Login";
import Register from "./pages/Register";

// Set new routes HERE
Vue.router = new VueRouter({
    hashbang: false,
    linkActiveClass: 'active',
    mode: 'history',
    base: __dirname,
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/view-template',
            name: 'viewtemplate',
            component: ViewTemplate
        },
        {
            path: '/users',
            name: 'users',
            component: Users,
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: {auth: false}
        },
        {
            path: '/register',
            name: 'register',
            component: Register,
            meta: {auth: false}
        },
    ]
});

// Http 
Vue.http.options.root = 'http://localhost';

Vue.use(require('@websanova/vue-auth'), {
    auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
    http: require('@websanova/vue-auth/drivers/http/vue-resource.1.x.js'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js')
});


export const app = new Vue({
    el: '#app',
    render: h => h(App),
    router: Vue.router
});

// // Start
// var component = App;

// component.router = Vue.router;

// new Vue(component).$mount('#app');

